export default {
  confirm: {
    cs: 'Tato akce je nevratná. Opravdu chcete smazat kategorii s názvem "%s"?',
    en: 'This action is permanent. Are you sure you want to delete category "%s"?'
  },
  forced: {
    cs: "Smazat kategorii i pokud obsahuje vtipy",
    en: "Delete category assigned to jokes"
  }
};
