import Lsi from "../config/lsi.js";

export default {
  list: {
    cs: "Seznam mol",
    en: "List of piers"
  },
  create: {
    cs: "Vytvořit molo",
    en: "Create pier"
  },
  createHeader: {
    cs: "Vytvořit molo",
    en: "Create pier"
  },
  updateHeader: {
    cs: "Upravit kategorii",
    en: "Update pier"
  },
  deleteHeader: {
    cs: "Smazat kategorii",
    en: "Delete pier"
  },
  ...Lsi.buttons
};
