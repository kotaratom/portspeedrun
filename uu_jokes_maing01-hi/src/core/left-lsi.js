export default {
  boats: {
    cs: "Lodě",
    en: "Botes"
  },
  categories: {
    cs: "Mola",
    en: "Piers"
  },
  about: {
    cs: "O aplikaci",
    en: "About Application"
  },
  login: {
    cs: "Přihlášení",
    en: "Login"
  }
};
