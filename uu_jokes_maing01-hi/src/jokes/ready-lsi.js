import Lsi from "../config/lsi.js";

export default {
  list: {
    cs: "Seznam lodí",
    en: "List of boats"
  },
  create: {
    cs: "Vytvořit loď",
    en: "Create boat"
  },
  createHeader: {
    cs: "Vytvořit loď",
    en: "Create boat"
  },
  updateHeader: {
    cs: "Upravit loď",
    en: "Update boat"
  },
  deleteHeader: {
    cs: "Smazat loď",
    en: "Delete boat"
  },

  deleteConfirm: {
    cs: 'Tato akce je nevratná. Opravdu chcete smazat loď s názvem "%s"?',
    en: 'This action is permanent. Are you sure you want to delete boat "%s"?'
  },
  ...Lsi.buttons,

  filterByCategory: {
    cs: "Mola",
    en: "Piers"
  },

  filterByImage: {
    cs: "Obrázku",
    en: "Image"
  },

  filterByUser: {
    cs: "Uživatele",
    en: "User"
  },

  filterByVisibility: {
    cs: "Publikace",
    en: "Published"
  },

  filterByRating: {
    cs: "Hodnocení",
    en: "Rating"
  }
};
