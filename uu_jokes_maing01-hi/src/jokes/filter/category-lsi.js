export default {
  category: {
    cs: "<uu5string/>Kategorie <UU5.Bricks.Strong>%s</UU5.Bricks.Strong>",
    en: "<uu5string/>Category <UU5.Bricks.Strong>%s</UU5.Bricks.Strong>"
  },
  apply: {
    en: "Apply",
    cs: "Použít"
  }
};
