export default {
  name: {
    cs: "Název",
    en: "Name"
  },
  text: {
    cs: "Text",
    en: "Text"
  },
  image: {
    cs: "Obrázek",
    en: "Image"
  },
  category: {
    cs: "Kategorie",
    en: "Category"
  },
  published: {
    cs: "Publikován",
    en: "Published"
  },
  textOrFile: {
    cs: "Text nebo obrázek musí být vyplněn.",
    en: "Text of image must be provided."
  }
};
