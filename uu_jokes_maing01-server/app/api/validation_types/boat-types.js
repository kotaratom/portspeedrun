/* eslint-disable */
const boatCreateDtoInType = shape({
  name: uu5String(255).isRequired(),
  boatNo: uu5String(255).isRequired(),
  color: uu5String(255),
  size: oneOf(["Small","Large"]),
  captainNo: uu5String(255),
  state: oneOf(["Docked","Undocked"])
});

const jokeGetDtoInType = shape({
  id: id().isRequired()
});

const boatUpdateDtoInType = shape({
  id: id().isRequired(),
  name: uu5String(255),
  color: uu5String(255),
  size: oneOf(["Small","Large"]),
  captainNo: uu5String(255),
  state: oneOf(["Docked","Undocked"])
});

const jokeUpdateVisibilityDtoInType = shape({
  id: id().isRequired(),
  visibility: boolean().isRequired()
});

const jokeDeleteDtoInType = shape({
  id: id().isRequired()
});

const jokeListDtoInType = shape({
  sortBy: oneOf(["name", "rating"]),
  order: oneOf(["asc", "desc"]),
  size : oneOf(["Small","Large"]),
  state : oneOf(["Docked","Undocked"]),
  pageInfo: shape({
    pageIndex: integer(),
    pageSize: integer()
  })
});

const jokeAddRatingDtoInType = shape({
  id: id().isRequired(),
  rating: integer(5).isRequired()
});
